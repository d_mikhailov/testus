﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Class.Domain.Enities 
{
    public class Role
    {
        public int id { get; set; }
        [StringLength(64)]
        public string name { get; set; }
        public virtual ICollection<User> users { get; set; } = new HashSet<User>();
    }
}
