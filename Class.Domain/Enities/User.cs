﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class.Domain.Enities 
{
    public class User
    {
        public int Id { get; set; }
        [EmailAddress]
        public string email { get; set; }
        [StringLength(64)]
        public string first_name { get; set; }
        [StringLength(64)]
        public string last_name { get; set; }
        [StringLength(64)]
        public string password { get; set; }
        
        public DateTime createdate { get; set; }
        // TODO: Refactor to DateTime
        public DateTime lastenter { get; set; }
        public DateTime birthday { get; set; }
        // TODO: Refactor to enum
        [StringLength(1)]
        public string genderlist { get; set; }
        // TODO: Refactor to bool
        public bool remove { get; set; }
        public virtual ICollection<Role> roles { get; set; } = new HashSet<Role>();

    }
}
