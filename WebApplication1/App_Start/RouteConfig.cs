﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(                                                                            
                name: "listofuser",
                url: "user/list",
                defaults: new {controller = "User", action = "UserList"}
                );
            routes.MapRoute(
                name: "listofroles",
                url: "role/list",
                defaults: new { controller = "Role", action = "roleList" }
                );
            routes.MapRoute(                                                                             
                    name: "createuser",
                    url: "createuser",
                    defaults: new { controller = "User", action = "CreateUser" }
                );
            routes.MapRoute(
                name: "user_information",
                url: "curentuser/{id}",
                defaults: new { controller = "User", action = "UserInformation"}
                );
            routes.MapRoute(
                    name: "change_user_inf",
                    url: "updateuserinf",
                    defaults: new { controller = "User", action = "ChangeUserInformation" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            

        }
    }
}
