﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.Entity;
using WebApplication1_db;
using System.Web.Mvc;
using Class.Domain.Enities;
using CustomInterface;
using System.Web;


namespace FunctionForActionWithDb
{
    public class UserAction : IUserActionInterface
    {
        // TODO: Refactor as constructor params
        public static string con_string = ConfigurationManager.ConnectionStrings["MsDatabase"].ConnectionString;
        Context db = new Context(con_string);


        //save changes for user
        public void RewriteUserData(FormCollection form)
        {

            var user_inf = db.Users.Find(Int16.Parse(form.Get("user_id")));

            user_inf.first_name = form.Get("first_name");
            user_inf.last_name = form.Get("last_name");
            user_inf.email = form.Get("email");
            user_inf.password = form.Get("password");
            user_inf.createdate = DateTime.Parse(form.Get("createdate"));            
            user_inf.birthday = DateTime.Parse(form.Get("birthday"));
            user_inf.roles.Clear();



            var all_roles = allRoles();
            bool state;
            foreach (var role in all_roles)
            {
                state = Convert.ToBoolean(form[role.name].Split(',')[0]);
                if (state)
                {

                    var role_object = from role_item in db.Roles where role_item.id == role.id select role_item;
                    var obj = db.Roles.Find(role.id);
                    user_inf.roles.Add(obj);

                }
            }
            //db.Users.Add(user_inf);
            db.SaveChanges();
            

        }
        //view all users
        public List<Role> allRoles()
        {
            var all_roles = from role in db.Roles select role;
            return all_roles.ToList();
        }
        public List<User> AllUser()
        {
            var all_user = from user_list in db.Users orderby user_list.first_name where user_list.remove == false select user_list;
            return all_user.ToList();



        }
        //delete current user
        public void RemoveUser(int user_id)
        {
            try
            {
                var user_inf = db.Users.Find(user_id);
                user_inf.remove = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
            }

        }
        //load information for current user
        public List<User> ShowUserWithId(int user_id)
        {

            var cur_user_inf = new List<User>();

            var user = db.Users
                .SingleOrDefault(u => u.Id == user_id);

            var df = db.Users.Include(r => r.roles).SingleOrDefault(u => u.Id == user_id);


            var user_inf = from user_list in db.Users where user_list.Id == user_id select user_list;

            return user_inf.ToList();

        }
        public void AddUserToDb(FormCollection col)
        {
            User user_inf = new User();
            Role roles = new Role();
            var role_list = new List<Role>();


            user_inf.first_name = col.Get("first_name");
            user_inf.last_name = col.Get("last_name");
            user_inf.password = col.Get("password");
            user_inf.email = col.Get("email");
            user_inf.createdate = DateTime.Today;
            user_inf.birthday = DateTime.Parse(col.Get("birthday"));
            user_inf.lastenter = DateTime.Today;
            user_inf.genderlist = col.Get("gender");
           // db.Users.Add(user_inf);
            //db.SaveChanges();

            var all_roles = allRoles();
            bool state;
            foreach (var role in all_roles)
            {
                state = Convert.ToBoolean(col[role.name].Split(',')[0]);
                if (state)
                {

                    var role_object = from role_item in db.Roles where role_item.id == role.id select role_item;
                    var obj = db.Roles.Find(role.id);
                    user_inf.roles.Add(obj);
                   
                }
            }

            db.Users.Add(user_inf);
            db.SaveChanges();


        }
    }
}