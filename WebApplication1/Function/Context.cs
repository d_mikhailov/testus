﻿using System.Data.Entity;
using Class.Domain.Enities;

namespace WebApplication1_db
{
    
    public class Context : DbContext
    {
        public Context() : base("MsDatabase")
        {
            Database.SetInitializer(
                new DropCreateDatabaseIfModelChanges<Context>());
        }

        public Context(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            Database.SetInitializer(
                new DropCreateDatabaseIfModelChanges<Context>());
        }
        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }
    }
}


