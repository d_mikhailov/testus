﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomInterface;
using FunctionForActionWithDb;
using CustomModelToView;

namespace WebApplication1.Controllers
{
    public class UserController : Controller
    {
        UserAction act_db = new UserAction();

        public ActionResult Index()
        {     
                return View();
        }
       
        public ActionResult CreateUser()
        { 
            return View(act_db.allRoles());
        }

 
        

        // TODO: Refactor to Get (int id); Create; Edit; Delete;


        public ActionResult UserList()
        {
            return View(act_db.AllUser());
        }
    
        public ActionResult UserInformation(int id)
        {
            return View(act_db.ShowUserWithId(id));
        }

        // TODO: for POST/PUT verbs refactor params to viewmodel
        [HttpPost]
        public ActionResult AddUserToDb(FormCollection form)
        {
            // Add validation
               act_db.AddUserToDb(form);

          
            return RedirectToAction("UserList");
        }
        [HttpGet]
        [ActionName("AddUserToDb")]
        public ActionResult AddUserToDbGet()
        {
            return RedirectToAction("UserList");
        }

        public ActionResult RemoveUser(int user_id)
        {
            act_db.RemoveUser(user_id);
            return RedirectToAction("UserList");
        }
        public ActionResult ChangeUserInformation(int user_id)
        {
            ModelData mod = new ModelData();
            mod.users = act_db.ShowUserWithId(user_id);
            mod.roles = act_db.allRoles();
           return View(mod);
        }

        [HttpPost]
        public ActionResult RewriteUserData(FormCollection form)
        {
            act_db.RewriteUserData(form);
            return RedirectToAction("UserList");
        }
        [HttpGet]
        [ActionName("RewriteUserData")]
        public ActionResult RewriteUserDataGet()
        {
            return RedirectToAction("UserList");
        }
    }
}