﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModelToView;
using FunctionForActionWithDb;


namespace WebApplication1.Controllers
{
    public class DefaultController : Controller
    {

            
        UserAction act_db = new UserAction();
        ModelData model_inf = new ModelData();
        
      

        public ActionResult Index()
        {
            var all_user = act_db.AllUser();
            var roles = act_db.allRoles();
            model_inf.users = all_user;
            model_inf.roles = roles;


            return View(model_inf);
        }

       
    }
}