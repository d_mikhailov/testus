﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunctionForActionWithDb;

namespace WebApplication1.Controllers
{
    public class RoleController : Controller
    {
        UserAction act_db = new UserAction();


        // GET: Role
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult roleList()
        {
            return View(act_db.allRoles());
        }
    }
}